#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
    unsigned char* key = malloc(HM_LEN * 2);
        
	if (entropy != NULL)
	{
		//Use hmac with sha512.
    	key = HMAC(EVP_sha512(), KDF_KEY, HM_LEN, (unsigned char*)entropy, entLen, NULL, NULL); 
        memcpy(K->hmacKey, key, HM_LEN);  
        memcpy(K->aesKey, key + HM_LEN, HM_LEN);   
	}
	else
	{
		randBytes(K->hmacKey, HM_LEN);
     	randBytes(K->aesKey, HM_LEN);
	}
	
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
    unsigned char* aes_ct = malloc(len);
	unsigned char* hmac;
	size_t ctLen;
	int nWritten = 0;
	
    //Encrypt the plaintext using AES-256 ctr mode encryption.
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	
	if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, IV))
		ERR_print_errors_fp(stderr);
	
	if (1 != EVP_EncryptUpdate(ctx, aes_ct, &nWritten, (unsigned char*)inBuf, len))
		ERR_print_errors_fp(stderr);
		
	EVP_CIPHER_CTX_free(ctx);
	
	//Concatenate the IV with the AES ciphertext.    
	memcpy(outBuf, IV, 16);
	memcpy(outBuf + 16, aes_ct, nWritten);

    //Use hmac with sha256.
	hmac = HMAC(EVP_sha256(), &K->hmacKey, HM_LEN, (unsigned char*)outBuf, (nWritten + 16), NULL, NULL);    
    
    //Concatenate the hmac result with the IV and AES ciphertext.
  	memcpy((outBuf + (16 + nWritten)), hmac, HM_LEN);
	
   //Get the number of bytes written.
    ctLen = nWritten + HM_LEN + 16;
    
    if (ctLen != (ske_getOutputLen(len)))
    {
    	fprintf(stderr, "Invalid number of bytes written.\n");	
    	return 1;
	}
	else
	{
		return ctLen; 
	}
	
	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */
	return 0; /* TODO: should return number of bytes written, which
	             hopefully matches ske_getOutputLen(...). */
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	/* TODO: write this.  Hint: mmap. */
	return 0;
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
    unsigned char* msg1 = malloc(len - HM_LEN);
	unsigned char* msg2 = malloc(HM_LEN);
	unsigned char* iv = malloc(16);
	unsigned char* ct = malloc(len - HM_LEN - 16);
	unsigned char* hmac;
	size_t ctLen = len - HM_LEN - 16;
	int hmacEquals = 0;
	int nWritten = 0;
	
	memcpy(msg1, inBuf, (len - HM_LEN));
	
	hmac = HMAC(EVP_sha256(), &K->hmacKey, HM_LEN, (unsigned char*)msg1, (len - HM_LEN), NULL, NULL);   
	
	memcpy(msg2, &inBuf[len - HM_LEN], HM_LEN);
	
	//Check if the two macs are equal.
	hmacEquals = memcmp(msg2, hmac, HM_LEN);
	
	if (hmacEquals != 0)
    {	
    	return -1;
	}
	
	//Decrypt the ciphertext using AES-256 ctr mode decryption.
	memcpy(iv, inBuf, 16);
	memcpy(ct, &inBuf[16], ctLen);
	
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	
	if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, iv))
		ERR_print_errors_fp(stderr);
		
	if (1 != EVP_DecryptUpdate(ctx, outBuf, &nWritten, ct, ctLen))
		ERR_print_errors_fp(stderr);

	return nWritten;
	
	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */
	return 0;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
	/* TODO: write this. */
	return 0;
}
